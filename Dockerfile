FROM openjdk
COPY target/*.jar /app.jar
CMD ["sudo chmod 777 /var/run/docker.sock"]
EXPOSE 8080
ENTRYPOINT ["java","-jar","/my-app-1.0-SNAPSHOT.jar" ]
